# frozen_string_literal: true

class Tournament < ApplicationRecord
  include CountryNameable

  has_many :tournament_team_players, inverse_of: :tournament, dependent: :destroy
  has_many :tournament_teams, through: :tournament_team_players, source: :team
  has_many :teams, inverse_of: :tournament, dependent: :nullify
  has_many :rounds, inverse_of: :tournament, dependent: :destroy
  has_many :matches, through: :rounds, source: :matches

  accepts_nested_attributes_for :tournament_team_players, allow_destroy: true, reject_if: :all_blank
end
