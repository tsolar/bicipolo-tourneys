# frozen_string_literal: true

module CountryNameable
  extend ActiveSupport::Concern

  # Assuming country_select is used with User attribute `country_code`
  # This will attempt to translate the country name and use the default
  # (usually English) name if no translation is available
  def country_name
    return if country.blank?

    country.translations[I18n.locale.to_s] || country.common_name || country.iso_short_name
  end

  def country_flag
    country&.emoji_flag
  end

  def country
    return if country_code.blank?

    ISO3166::Country[country_code]
  end
end
