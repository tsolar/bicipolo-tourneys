# frozen_string_literal: true

class Profile < ApplicationRecord
  include CountryNameable

  GENDERS = %w[
    female
    male
    other
  ].freeze

  enum(:gender, GENDERS.to_h { |h| [h, h] })

  SHIRT_SIZES = %w[
    XXS
    XS
    S
    M
    L
    XL
    XXL
    XXXL
  ].freeze

  delegate :name, to: :club, prefix: true, allow_nil: true

  belongs_to :club, optional: true
  belongs_to :user, optional: true
  has_one :player, inverse_of: :profile, dependent: :destroy
  # TODO: should validate a profile can't be deleted if players are associated to tournaments

  has_many :identification_documents, inverse_of: :profile, dependent: :destroy

  validates :first_name, presence: true

  def name
    [
      first_name,
      middle_name,
      last_name,
      second_last_name
    ].join(' ')
  end

  def display_name
    first_name || nickname
  end
end
