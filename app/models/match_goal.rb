# frozen_string_literal: true

class MatchGoal < ApplicationRecord
  belongs_to :match
  belongs_to :tournament_team_player

  after_create :add_score_to_match

  private

  def add_score_to_match
    match.increment(team_score, 1) if team_score.present?
    match.save! # TODO: check if there's any exception it should not create the MatchGoal
  end

  def team_score
    return :home_score if tournament_team_player.team == match.home_team

    :visitor_score if tournament_team_player.team == match.visitor_team
  end
end
