# frozen_string_literal: true

class Player < ApplicationRecord
  has_many :tournament_team_players, inverse_of: :player, dependent: :destroy

  belongs_to :profile, inverse_of: :player

  accepts_nested_attributes_for :profile

  # delegate national flag to profile
  delegate :country_flag, to: :profile, allow_nil: true
  delegate :gender, to: :profile, allow_nil: true
  delegate :name, to: :profile, prefix: true, allow_nil: true

  def national_flag
    country_flag
  end

  def name
    profile_name || super
  end
end
