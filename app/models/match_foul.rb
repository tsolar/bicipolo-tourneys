# frozen_string_literal: true

class MatchFoul < ApplicationRecord
  belongs_to :match
  belongs_to :tournament_team_player
end
