# frozen_string_literal: true

class Team < ApplicationRecord
  belongs_to :tournament, optional: true, inverse_of: :teams
  has_many :tournament_team_players, inverse_of: :team, dependent: :destroy

  accepts_nested_attributes_for :tournament_team_players, allow_destroy: true, reject_if: :all_blank # , limit: 4

  delegate :name, to: :tournament, prefix: true
  delegate :team_players_limit, to: :tournament, prefix: true

  # validates :tournament_team_players, length: { maximum: :tournament_team_players_limit }

  validates :name, presence: true # TODO: add uniqueness

  def players_display_names
    return '-' if tournament_team_players.empty?

    tournament_team_players.map(&:display_name).join(', ')
  end
end
