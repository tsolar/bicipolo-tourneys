# frozen_string_literal: true

class Round < ApplicationRecord
  belongs_to :tournament, inverse_of: :rounds
  has_many :matches, inverse_of: :round, dependent: :destroy
end
