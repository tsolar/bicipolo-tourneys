# frozen_string_literal: true

class IdentificationDocument < ApplicationRecord
  belongs_to :identification_document_type
  belongs_to :profile, inverse_of: :identification_documents
end
