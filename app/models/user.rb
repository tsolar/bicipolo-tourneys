# frozen_string_literal: true

class User < ApplicationRecord
  OMNIAUTH_PROVIDERS = %i[google_oauth2 facebook].freeze

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable,
         :confirmable, :lockable,
         :omniauthable, omniauth_providers: OMNIAUTH_PROVIDERS

  has_one :profile
  delegate :preferred_locale, to: :profile, allow_nil: true

  def display_name
    profile&.display_name || email
  end

  def first_and_last_name
    return email unless profile

    name = "#{profile.first_name} #{profile.last_name}".squish

    return email if name.blank?

    name
  end

  # TODO: create a separate method or each provider
  def self.from_omniauth(access_token)
    data = access_token.info
    # rubocop: disable Style/RedundantAssignment
    user = User.where(email: data['email']).first

    # Uncomment the section below if you want users to be created if they don't exist
    # if user.blank?

    #   user = User.create( # name: data['name'],
    #     email: data['email'],
    #     password: Devise.friendly_token[0,20]
    #   )
    # end
    user
    # rubocop: enable Style/RedundantAssignment
  end

  protected

  def send_devise_notification(notification, *args)
    if Rails.env.development?
      devise_mailer.send(notification, self, *args).deliver_now
      return
    end

    devise_mailer.send(notification, self, *args).deliver_later(queue: 'mailers')
  end
end
