# frozen_string_literal: true

class Match < ApplicationRecord
  belongs_to :round
  has_one :tournament, through: :round
  belongs_to :home_team, class_name: 'Team'
  belongs_to :visitor_team, class_name: 'Team'

  has_many :match_goals, inverse_of: :match, dependent: :destroy
  has_many :match_fouls, inverse_of: :match, dependent: :destroy

  delegate :name, to: :round, prefix: true
  delegate :name, to: :home_team, prefix: true
  delegate :name, to: :visitor_team, prefix: true

  def add_home_goal(player: nil, time: nil, own_goal: false)
    add_goal(team: home_team, player: player, time: time, own_goal: own_goal)
  end

  def add_visitor_goal(player: nil, time: nil, own_goal: false)
    add_goal(team: visitor_team, player: player, time: time, own_goal: own_goal)
  end

  def add_goal(team:, player: nil, time: nil, own_goal: false)
    match_goals.create(
      tournament_team_player: tournament_team_player(team:, player:),
      time: time,
      own_goal: own_goal
    )
  end

  def substract_home_goal
    # pending. Maybe non-sense because I need to know which goal to remove, and
    # I can know the goal by ID, so just destroy the goal record by ID.
  end

  def substract_visitor_goal
    # pending. Maybe non-sense because I need to know which goal to remove, and
    # I can know the goal by ID, so just destroy the goal record by ID.
  end

  def substract_goal
    # pending. Maybe non-sense because I need to know which goal to remove, and
    # I can know the goal by ID, so just destroy the goal record by ID.
  end

  def tournament_team_player(team:, player:)
    TournamentTeamPlayer.find_by(
      team:,
      player:,
      tournament: # get this with belongs_to through?
    )
  end
end
