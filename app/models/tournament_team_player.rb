# frozen_string_literal: true

class TournamentTeamPlayer < ApplicationRecord
  belongs_to :tournament, inverse_of: :tournament_team_players
  belongs_to :team, optional: true, inverse_of: :tournament_team_players
  belongs_to :player, inverse_of: :tournament_team_players

  accepts_nested_attributes_for :player

  # TODO: I want to use this but it's working weird?
  # validates_associated :team
  # validates_associated :tournament

  # TODO: check this
  # validate :check_tournament_team_players_limit

  delegate :team_players_limit, to: :tournament, prefix: true, allow_nil: true

  def display_name
    [
      player.national_flag,
      "(#{player.gender.presence || 'unknown'})",
      player.name
    ].join(' ')
  end

  private

  def check_tournament_team_players_limit
    # debugger
    return true if tournament_team_players_limit.blank?
    return true if tournament_team_players_limit < 1 # 0 for unlimited players on a team

    # return true if (team.tournament_team_players.size <= tournament.team_players_limit) && team.tournament_team_players.exclude?(self)
    # this works but maybe needs a refactor
    return true if !self_in_team_players? && !team_players_limit_reached?

    errors.add(
      :base,
      :invalid,
      message: "Team players limit for the tournament is #{tournament.team_players_limit}"
    )
  end

  def self_in_team_players?
    return false if team.blank?

    return false unless team.persisted?

    Team.find(team_id).tournament_team_players.include?(self)
  end

  def team_players_limit_reached?
    return false if team.blank?

    # I need count from sql query, i.e. persisted team players
    team.tournament_team_players.count >= tournament.team_players_limit
  end
end
