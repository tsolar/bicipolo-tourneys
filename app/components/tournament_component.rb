# frozen_string_literal: true

class TournamentComponent < ViewComponent::Base
  def initialize(tournament:)
    @tournament = tournament
  end
end
