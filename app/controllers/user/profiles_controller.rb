# frozen_string_literal: true

class User::ProfilesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_user_profile

  def show
  end

  def edit
  end

  def update
    respond_to do |format|
      if @user_profile.update(user_profile_params)
        format.html { redirect_to user_profile_url, notice: 'Profile was successfully updated.' }
        format.json { render :show, status: :ok, location: @user_profile }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @user_profile.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_user_profile
    @user_profile = current_user.profile || current_user.build_profile
  end

  # Only allow a list of trusted parameters through.
  def user_profile_params
    params.require(:profile).permit(
      :first_name,
      :middle_name,
      :last_name,
      :second_last_name,
      :nickname,
      :dob,
      :country_code,
      :city,
      :club_id,
      :shirt_size,
      :lefty,
      :gender,
      :other_gender,
      :pronouns,
      :food_preferences,
      :allergies,
      :preferred_locale
    )
  end
end
