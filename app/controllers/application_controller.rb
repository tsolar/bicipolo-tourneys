# frozen_string_literal: true

class ApplicationController < ActionController::Base
  around_action :switch_locale

  def switch_locale(&)
    locale = current_user.try(:preferred_locale) || params_locale || I18n.default_locale
    I18n.with_locale(locale, &)
  end

  def default_url_options(*args)
    return super if params_locale_is_default_locale?

    super.merge(
      locale: params_locale
    )
  end

  protected

  def params_locale
    sym_locale = params[:locale]&.to_sym
    return I18n.default_locale unless sym_locale.in?(I18n.available_locales)

    sym_locale
  end

  def params_locale_is_default_locale?
    params_locale == I18n.default_locale
  end
end
