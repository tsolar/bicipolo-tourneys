# frozen_string_literal: true

class TeamsController < ApplicationController
  before_action :set_tournament,
                only: %i[index show new create edit update destroy register create_from_register registration_done]
  before_action :set_team, only: %i[show edit update destroy registration_done]

  # GET /teams or /teams.json
  def index
    @teams = @tournament.teams.includes(tournament_team_players: { player: :profile }).all
  end

  # GET /teams/1 or /teams/1.json
  def show
  end

  # GET /teams/new
  def new
    @team = Team.new(tournament: @tournament)
  end

  # GET /teams/1/edit
  def edit
  end

  # POST /teams or /teams.json
  def create
    @team = Team.new(team_params)

    respond_to do |format|
      if @team.save
        format.html { redirect_to tournament_team_url(@tournament, @team), notice: 'Team was successfully created.' }
        format.json { render :show, status: :created, location: @team }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @team.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /teams/1 or /teams/1.json
  def update
    respond_to do |format|
      if @team.update(team_params)
        format.html { redirect_to tournament_team_url(@tournament, @team), notice: 'Team was successfully updated.' }
        format.json { render :show, status: :ok, location: @team }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @team.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /teams/1 or /teams/1.json
  def destroy
    @team.destroy

    respond_to do |format|
      format.html { redirect_to tournament_teams_url, notice: 'Team was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def register
    @team = Team.new(tournament: @tournament)
    # debugger
    3.times do
      @team.tournament_team_players.new(
        player: Player.new(profile: Profile.new),
        tournament: @tournament,
        team: @team
      )
    end
  end

  def create_from_register
    @team = Team.new(tournament_team_registration_params)
    @team.tournament_team_players.each do |ttp|
      ttp.tournament_id = @team.tournament_id
    end

    respond_to do |format|
      if @team.save
        format.html do
          redirect_to registration_done_tournament_team_url(@team.tournament, @team),
                      notice: 'Team was successfully registered.'
        end
        format.json { render :show, status: :ok, location: @team }
      else
        format.html { render :register, status: :unprocessable_entity }
        format.json { render json: @team.errors, status: :unprocessable_entity }
      end
    end
  end

  def registration_done
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_team
    # TODO: replace with @tournament.teams.find(...)
    @team = Team.includes(:tournament).find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def team_params
    params.require(:team).permit(:name, :tournament_id)
  end

  def set_tournament
    @tournament = Tournament.find(params[:tournament_id])
  end

  def tournament_team_registration_params
    pars = params.require(:team).permit(
      :name,
      :tournament_id,
      tournament_team_players_attributes: [
        :id,
        :_destroy,
        :tournament_id,
        :captain,
        { team_attributes: %i[
            id
            name
          ],
          player_attributes: [
            :id,
            { profile_attributes: %i[
              id
              first_name
              middle_name
              last_name
              second_last_name
              nickname
              dob
              nationality_country
              country_code
              city
              club_id
              lefty
              gender
              shirt_size
              other_gender
              pronouns
            ] }
          ] }
      ]
    )

    pars.merge(tournament_id: params[:tournament_id])
  end
end
