# frozen_string_literal: true

json.extract! match, :id, :round_id, :home_team_id, :visitor_team_id, :home_score, :visitor_score, :created_at,
              :updated_at
json.url match_url(match, format: :json)
