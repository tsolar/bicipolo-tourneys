# frozen_string_literal: true

json.extract! round, :id, :name, :order, :tournament_id, :created_at, :updated_at
json.url round_url(round, format: :json)
