# frozen_string_literal: true

json.array! @rounds, partial: 'rounds/round', as: :round
