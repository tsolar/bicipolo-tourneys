# frozen_string_literal: true

json.extract! user_profile, :id, :first_name, :middle_name, :last_name, :second_last_name, :nickname, :dob, :country_code, :city, :club_id, :shirt_size, :lefty, :gender, :other_gender, :pronouns, :created_at, :updated_at, :user_id, :created_at, :updated_at
json.url user_profile_url(user_profile, format: :json)
