# frozen_string_literal: true

json.partial! 'user/profiles/user_profile', user_profile: @user_profile
