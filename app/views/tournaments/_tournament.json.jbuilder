# frozen_string_literal: true

json.extract! tournament, :id, :name, :description, :starts_at, :ends_at, :address, :city, :country_code, :latitude,
              :longitude, :created_at, :updated_at
json.url tournament_url(tournament, format: :json)
