import { Controller } from "@hotwired/stimulus"

// Connects to data-controller="matches"
export default class extends Controller {
  connect() {
    // console.log(this.element)
  }

  addGoal(event) {
    event.preventDefault()
    // console.log(event.params)
    const tournament_team_player_id = event.params.tournamentTeamPlayerId
    const matchId = event.params.matchId
    const tournamentId = event.params.tournamentId
    const csrfToken = document.querySelector("[name='csrf-token']").content
    // debugger
    fetch(`/tournaments/${tournamentId}/matches/${matchId}/add_goal`, {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
                'Content-Type': 'application/json',
                'X-CSRF-Token': csrfToken
            },
            body: JSON.stringify({
              tournament_team_player_id,
            }) // body data type must match "Content-Type" header
    }).then(response => response.json())
      .then(data => {
        const homeTeamScoreValueElement = document.querySelector(".home_team .score .value")
        const visitorTeamScoreValueElement = document.querySelector(".visitor_team .score .value")
        homeTeamScoreValueElement.innerText = ` ${data.home_score}`
        visitorTeamScoreValueElement.innerText = ` ${data.visitor_score}`
      })
  }

  add(event) {
    debugger
    event.preventDefault()
    console.log(" paso por add")
  }
}
