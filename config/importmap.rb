# frozen_string_literal: true

# Pin npm packages by running ./bin/importmap

pin 'application', preload: true
pin '@hotwired/turbo-rails', to: 'turbo.min.js', preload: true
pin '@hotwired/stimulus', to: 'stimulus.min.js', preload: true
pin '@hotwired/stimulus-loading', to: 'stimulus-loading.js', preload: true
pin_all_from 'app/javascript/controllers', under: 'controllers'
# pin 'flowbite', to: 'https://ga.jspm.io/npm:flowbite@1.5.2/dist/flowbite.js'
pin 'flowbite', to: 'https://cdn.jsdelivr.net/npm/flowbite@2.5.1/dist/flowbite.turbo.min.js'
# pin 'flowbite', to: './node_modules/flowbite'
pin 'vanilla-nested', to: 'vanilla_nested.js', preload: true
