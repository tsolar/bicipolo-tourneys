# frozen_string_literal: true

require 'sidekiq/web'

Rails.application.routes.draw do
  mount Sidekiq::Web => '/sidekiq'
  root 'home#index'
  get 'home/index', to: 'home#index'

  namespace :user do
    get 'profile', to: 'profiles#show'
    get 'profile/edit', to: 'profiles#edit'
    put 'profile', to: 'profiles#update'
  end

  devise_scope :user do
    get :users, to: 'devise/sessions#new'
  end
  devise_for :users, controllers: { omniauth_callbacks: 'users/omniauth_callbacks' }

  resources :clubs
  resources :rounds
  resources :players
  resources :tournaments do
    resources :matches do
      member do
        post :add_goal # should be MatchGoal nested resource?
      end
    end
    resources :teams do
      collection do
        get :register
        post :create_from_register
      end

      member do
        get :registration_done
      end
    end
  end
end
