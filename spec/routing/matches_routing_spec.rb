# frozen_string_literal: true

require 'rails_helper'

RSpec.describe MatchesController do
  describe 'routing' do
    it 'routes to #index' do
      pending 'Matches are nested in tournaments'
      expect(get: '/matches').to route_to('matches#index')
    end

    it 'routes to #new' do
      pending 'Matches are nested in tournaments'
      expect(get: '/matches/new').to route_to('matches#new')
    end

    it 'routes to #show' do
      pending 'Matches are nested in tournaments'
      expect(get: '/matches/1').to route_to('matches#show', id: '1')
    end

    it 'routes to #edit' do
      pending 'Matches are nested in tournaments'
      expect(get: '/matches/1/edit').to route_to('matches#edit', id: '1')
    end

    it 'routes to #create' do
      pending 'Matches are nested in tournaments'
      expect(post: '/matches').to route_to('matches#create')
    end

    it 'routes to #update via PUT' do
      pending 'Matches are nested in tournaments'
      expect(put: '/matches/1').to route_to('matches#update', id: '1')
    end

    it 'routes to #update via PATCH' do
      pending 'Matches are nested in tournaments'
      expect(patch: '/matches/1').to route_to('matches#update', id: '1')
    end

    it 'routes to #destroy' do
      pending 'Matches are nested in tournaments'
      expect(delete: '/matches/1').to route_to('matches#destroy', id: '1')
    end
  end
end
