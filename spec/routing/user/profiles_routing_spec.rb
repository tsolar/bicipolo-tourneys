# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User::ProfilesController do
  describe 'routing' do
    it 'routes to #show' do
      expect(get: '/user/profile').to route_to('user/profiles#show')
    end

    it 'routes to #edit' do
      expect(get: '/user/profile/edit').to route_to('user/profiles#edit')
    end

    it 'routes to #update via PUT' do
      expect(put: '/user/profile').to route_to('user/profiles#update')
    end
  end
end
