# frozen_string_literal: true

require 'rails_helper'

RSpec.describe TeamsController do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/tournaments/1/teams').to route_to('teams#index', tournament_id: '1')
    end

    it 'routes to #new' do
      expect(get: '/tournaments/1/teams/new').to route_to('teams#new', tournament_id: '1')
    end

    it 'routes to #show' do
      expect(get: '/tournaments/2/teams/1').to route_to('teams#show', id: '1', tournament_id: '2')
    end

    it 'routes to #edit' do
      expect(get: '/tournaments/2/teams/1/edit').to route_to('teams#edit', id: '1', tournament_id: '2')
    end

    it 'routes to #create' do
      expect(post: '/tournaments/2/teams').to route_to('teams#create', tournament_id: '2')
    end

    it 'routes to #update via PUT' do
      expect(put: '/tournaments/2/teams/1').to route_to('teams#update', id: '1', tournament_id: '2')
    end

    it 'routes to #update via PATCH' do
      expect(patch: '/tournaments/2/teams/1').to route_to('teams#update', id: '1', tournament_id: '2')
    end

    it 'routes to #destroy' do
      expect(delete: '/tournaments/2/teams/1').to route_to('teams#destroy', id: '1', tournament_id: '2')
    end
  end
end
