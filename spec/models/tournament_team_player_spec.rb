# frozen_string_literal: true

require 'rails_helper'

RSpec.describe TournamentTeamPlayer do
  subject(:tournament_team_player) { build(:tournament_team_player) }

  it { is_expected.to have_db_column(:captain).of_type(:boolean) }

  it do
    expect(tournament_team_player).to belong_to(:tournament)
      .inverse_of(:tournament_team_players)
  end

  it do
    expect(tournament_team_player).to belong_to(:team)
      .optional(true)
      .inverse_of(:tournament_team_players)
  end

  it do
    expect(tournament_team_player).to belong_to(:player)
      .inverse_of(:tournament_team_players)
  end

  it 'has a valid factory' do
    expect(tournament_team_player).to be_valid
  end
end
