# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Match do
  it { is_expected.to belong_to(:round) }
  it { is_expected.to have_one(:tournament).through(:round) }
  it { is_expected.to belong_to(:home_team).class_name('Team') }
  it { is_expected.to belong_to(:visitor_team).class_name('Team') }

  it { is_expected.to have_many(:match_goals).inverse_of(:match).dependent(:destroy) }
  it { is_expected.to have_many(:match_fouls).inverse_of(:match).dependent(:destroy) }

  it { is_expected.to delegate_method(:name).to(:round).with_prefix(true) }
  it { is_expected.to delegate_method(:name).to(:home_team).with_prefix(true) }
  it { is_expected.to delegate_method(:name).to(:visitor_team).with_prefix(true) }

  describe '#add_goal' do
    subject(:add_goal_call) do
      match.add_goal(
        team:,
        player:,
        time:,
        own_goal:
      )
      # debugger
      # byebug
    end

    let(:tournament) { create(:tournament) }
    let(:match) do
      create(
        :match,
        round:,
        home_team:,
        visitor_team:
      )
    end
    let(:round) { create(:round, tournament:) }
    let(:home_team) do
      team = create(:team, tournament:)
      create_list(
        :tournament_team_player,
        3,
        tournament:,
        team:
      )
      team
    end
    let(:visitor_team) { create(:team, tournament:) }
    let(:time) { '01:25' }
    let(:own_goal) { false }

    context 'when team is home_team' do
      let(:team) { home_team }
      let(:player) { home_team.tournament_team_players.first.player }

      it 'creates a MatchGoal' do
        expect { add_goal_call }.to change(MatchGoal, :count).by(1)
        expect(
          MatchGoal.includes(:tournament_team_player)
            .where(tournament_team_players: { team: home_team })
            .count
        ).to eq(1)
      end

      it 'does not create a MatchGoal for the visitor team' do
        add_goal_call
        expect(
          MatchGoal.includes(:tournament_team_player)
            .where(tournament_team_players: { team: visitor_team })
            .count
        ).to eq(0)
      end
    end
  end
end
