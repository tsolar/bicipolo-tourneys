# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Profile do
  subject(:profile) { build(:profile) }

  it { is_expected.to belong_to(:club).optional }
  it { is_expected.to have_one(:player).inverse_of(:profile).dependent(:destroy) }
  # it { is_expected.to belong_to(:user).inverse_of(:profile).dependent(:destroy) } # or nullify?
  it { is_expected.to delegate_method(:name).to(:club).with_prefix(true).allow_nil }

  it do
    expect(profile).to have_many(:identification_documents)
      .inverse_of(:profile)
      .dependent(:destroy)
  end

  it { is_expected.to have_db_column(:gender).of_type(:string) }
  it { is_expected.to have_db_column(:other_gender).of_type(:string) }
  it { is_expected.to have_db_column(:pronouns).of_type(:string) }
  it { is_expected.to have_db_column(:lefty).of_type(:boolean) }
  it { is_expected.to have_db_column(:city).of_type(:string) }
  it { is_expected.to have_db_column(:country_code).of_type(:string) }
  it { is_expected.to have_db_column(:dob).of_type(:date) }
  it { is_expected.to have_db_column(:shirt_size).of_type(:string) }

  it do
    expect(profile).to define_enum_for(:gender)
      .with_values(
        described_class::GENDERS.to_h { |h| [h, h] }
      )
      .backed_by_column_of_type(:string)
  end

  it { is_expected.to validate_presence_of(:first_name) }

  it 'has a valid factory' do
    expect(profile).to be_valid
  end
end
