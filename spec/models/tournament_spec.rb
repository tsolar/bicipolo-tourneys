# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Tournament do
  subject(:tournament) { build(:tournament) }

  it { is_expected.to have_db_column(:team_players_limit).of_type(:integer) }

  # TODO: nullify or restrict_with_*?
  # The team should not be deleted, but it should be reassigned maybe.
  it do
    expect(tournament).to have_many(:teams)
      .dependent(:nullify)
      .inverse_of(:tournament)
  end

  it do
    expect(tournament).to have_many(:tournament_team_players)
      .inverse_of(:tournament)
      .dependent(:destroy)
  end

  it do
    expect(tournament).to have_many(:tournament_teams)
      .through(:tournament_team_players)
      .source(:team)
  end

  it do
    expect(tournament).to accept_nested_attributes_for(:tournament_team_players)
      .allow_destroy(true)
    # .reject_if(:all_blank) # TODO: change this condition
  end

  it 'has a valid factory' do
    expect(tournament).to be_valid
  end
end
