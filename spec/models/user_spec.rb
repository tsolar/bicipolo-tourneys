# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User do
  subject(:user) { build(:user) }

  it do
    expect(user).to delegate_method(:preferred_locale)
      .to(:profile)
      .allow_nil
  end

  it 'has a valid factory' do
    expect(user).to be_valid
  end

  describe '#display_name' do
    subject { user.display_name }

    context 'when profile is nil' do
      let(:user) { build(:user, profile: nil) }

      it { is_expected.to eq(user.email) }
    end

    context 'when profile is present' do
      let(:user) { build(:user, :with_profile) }

      it { is_expected.to eq(user.profile.display_name) }
    end
  end

  describe '#first_and_last_name' do
    subject(:method) { user.first_and_last_name }

    let(:user) { build(:user) }

    context 'when profile is present' do
      let(:first_name) { Faker::Name.first_name }
      let(:last_name) { Faker::Name.last_name }
      let(:profile) { build(:profile, first_name:, last_name:) }
      let(:user) { build(:user, profile:) }

      context 'when first name is present and no last name' do
        let(:last_name) { nil }

        it 'returns only first name' do
          expect(method).to eq(profile.first_name)
        end
      end

      context 'when last name is present and no first name' do
        let(:first_name) { nil }

        it 'returns only last name' do
          expect(method).to eq(profile.last_name)
        end
      end

      context 'when first name and last name are present' do
        it 'returns first name and last name separated by a space' do
          expect(method).to eq("#{profile.first_name} #{profile.last_name}")
        end
      end
    end

    context 'when profile is not present' do
      it 'returns email' do
        expect(user.first_and_last_name).to eq(user.email)
      end
    end
  end
end
