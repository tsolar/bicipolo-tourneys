# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Player do
  subject(:player) { build(:player) }

  it do
    expect(player).to have_many(:tournament_team_players)
      .inverse_of(:player)
      .dependent(:destroy)
  end
end
