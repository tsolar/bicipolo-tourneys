# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Team do
  subject(:team) { build(:team) }

  it do
    expect(team).to delegate_method(:name)
      .to(:tournament)
      .with_prefix(true)
  end

  it do
    expect(team).to have_many(:tournament_team_players)
      .inverse_of(:team)
      .dependent(:destroy)
  end

  it { is_expected.to validate_presence_of(:name) }
end
