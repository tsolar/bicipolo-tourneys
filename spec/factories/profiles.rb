# frozen_string_literal: true

FactoryBot.define do
  factory :profile do
    user_id { create(:user).id }
    first_name { Faker::Name.first_name }
    middle_name { Faker::Name.middle_name }
    last_name { Faker::Name.last_name }
    second_last_name { Faker::Name.last_name }
    nickname { first_name&.parameterize(separator: '_') }
    dob { Faker::Date.birthday(min_age: 18, max_age: 65) }
    country_code { Faker::Address.country_code }
    city { Faker::Address.city }
    club_id { create(:club).id }
    shirt_size { 'M' }
    lefty { false }
    gender { Profile::GENDERS.sample }
    # TODO: emails, phone_numbers

    trait :lefty do
      lefty { true }
    end

    # identification_documents do
    #   [association(:identification_document, factory: :chilean_identification_document, profile: nil)]
    # end
    transient do
      identification_documents_count { 1 }
    end

    after(:create) do |profile, evaluator|
      create_list(
        :chilean_identification_document,
        evaluator.identification_documents_count,
        profile:
      )
    end
  end
end
