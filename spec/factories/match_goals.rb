# frozen_string_literal: true

FactoryBot.define do
  factory :match_goal do
    match { nil }
    tournament_team_player { nil }
    time { 'MyString' }
    own_goal { false }
  end
end
