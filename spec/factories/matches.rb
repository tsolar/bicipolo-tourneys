# frozen_string_literal: true

FactoryBot.define do
  factory :match do
    round { nil }
    home_team { nil }
    visitor_team { nil }
    home_score { 0 }
    visitor_score { 0 }
  end
end
