# frozen_string_literal: true

FactoryBot.define do
  factory :tournament_team_player do
    tournament
    team { association :team, tournament: }
    player
    captain { false }

    trait :captain do
      captain { true }
    end
  end
end
