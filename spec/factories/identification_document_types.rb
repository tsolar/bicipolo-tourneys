# frozen_string_literal: true

FactoryBot.define do
  factory :identification_document_type do
    sequence(:name) { |n| "My ID type #{n}" }
    internal_name { name.parameterize(separator: '_') }
    country_code { Faker::Address.country_code }
    validation_regex { '' }
    format_example { '' }

    trait :rut do
      name { 'RUT' }
      country_code { 'CL' }
      format_example { '11.111.111-1' }
    end

    trait :passport do
      name { 'Passport' }
      country_code { nil }
      validation_regex { '\A[a-zA-Z0-9]{6,9}\z' }
      format_example { '012345678' }
    end
  end
end
