# frozen_string_literal: true

FactoryBot.define do
  factory :match_foul do
    match { nil }
    tournament_team_player { nil }
    time { 'MyString' }
    offensive { false }
    foul_type { 'MyString' }
    foul_order { 'MyString' }
    seconds_out { 'MyString' }
  end
end
