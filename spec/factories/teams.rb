# frozen_string_literal: true

FactoryBot.define do
  factory :team do
    sequence(:name) { |n| "Team #{n}" }
    tournament

    trait :invalid do
      name { nil }
    end
  end
end
