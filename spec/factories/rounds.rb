# frozen_string_literal: true

FactoryBot.define do
  factory :round do
    name { 'MyString' }
    order { 1 }
    tournament { nil }
  end
end
