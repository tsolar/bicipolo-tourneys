# frozen_string_literal: true

FactoryBot.define do
  factory :identification_document do
    profile
    identification_document_type
    number { Faker::IDNumber.valid }

    factory :chilean_identification_document do
      number { Faker::ChileRut.full_rut }
      identification_document_type do
        rut = IdentificationDocumentType.find_by(name: 'RUT')
        rut || create(:identification_document_type, :rut)
      end
    end
  end
end
