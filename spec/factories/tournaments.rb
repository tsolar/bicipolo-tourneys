# frozen_string_literal: true

FactoryBot.define do
  factory :tournament do
    name { Faker::Lorem.sentence }
    description { Faker::Lorem.paragraph }
    starts_at { Time.current }
    ends_at { starts_at + 1.day }
    address { Faker::Address.full_address }
    city { Faker::Address.city }
    country_code { Faker::Address.country_code }
    latitude { Faker::Address.latitude }
    longitude { Faker::Address.longitude }
    team_players_limit { 3 }
  end
end
