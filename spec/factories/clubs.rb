# frozen_string_literal: true

FactoryBot.define do
  factory :club do
    sequence(:name) { |n| "#{country_code} Club #{n}" }
    country_code { Faker::Address.country_code }
    city { Faker::Address.city }
  end
end
