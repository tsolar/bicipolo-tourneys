# frozen_string_literal: true

FactoryBot.define do
  factory :player do
    name { Faker::Name.name }
    nickname { name.parameterize(separator: '_') }
    profile
  end
end
