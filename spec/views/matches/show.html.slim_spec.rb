# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'matches/show' do
  before do
    pending
    @match = assign(:match, Match.create!(
      round: nil,
      home_team: nil,
      visitor_team: nil,
      home_score: 2,
      visitor_score: 3
    ))
  end

  it 'renders attributes in <p>' do
    render
    expect(rendered).to match(//)
    expect(rendered).to match(//)
    expect(rendered).to match(//)
    expect(rendered).to match(/2/)
    expect(rendered).to match(/3/)
  end
end
