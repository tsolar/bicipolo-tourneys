# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'matches/new' do
  before do
    pending
    assign(:match, Match.new(
      round: nil,
      home_team: nil,
      visitor_team: nil,
      home_score: 1,
      visitor_score: 1
    ))
  end

  it 'renders new match form' do
    render

    assert_select 'form[action=?][method=?]', matches_path, 'post' do
      assert_select 'input[name=?]', 'match[round_id]'

      assert_select 'input[name=?]', 'match[home_team_id]'

      assert_select 'input[name=?]', 'match[visitor_team_id]'

      assert_select 'input[name=?]', 'match[home_score]'

      assert_select 'input[name=?]', 'match[visitor_score]'
    end
  end
end
