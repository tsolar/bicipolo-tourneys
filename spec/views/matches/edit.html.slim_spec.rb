# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'matches/edit' do
  before do
    pending
    @match = assign(:match, Match.create!(
      round: nil,
      home_team: nil,
      visitor_team: nil,
      home_score: 1,
      visitor_score: 1
    ))
  end

  it 'renders the edit match form' do
    render

    assert_select 'form[action=?][method=?]', match_path(@match), 'post' do
      assert_select 'input[name=?]', 'match[round_id]'

      assert_select 'input[name=?]', 'match[home_team_id]'

      assert_select 'input[name=?]', 'match[visitor_team_id]'

      assert_select 'input[name=?]', 'match[home_score]'

      assert_select 'input[name=?]', 'match[visitor_score]'
    end
  end
end
