# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'clubs/edit' do
  before do
    @club = assign(:club, Club.create!(
      name: 'MyString',
      country_code: 'MyString',
      city: 'MyString'
    ))
  end

  it 'renders the edit club form' do
    render

    assert_select 'form[action=?][method=?]', club_path(@club), 'post' do
      assert_select 'input[name=?]', 'club[name]'

      assert_select 'input[name=?]', 'club[country_code]'

      assert_select 'input[name=?]', 'club[city]'
    end
  end
end
