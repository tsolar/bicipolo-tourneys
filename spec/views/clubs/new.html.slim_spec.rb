# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'clubs/new' do
  before do
    assign(:club, Club.new(
      name: 'MyString',
      country_code: 'MyString',
      city: 'MyString'
    ))
  end

  it 'renders new club form' do
    render

    assert_select 'form[action=?][method=?]', clubs_path, 'post' do
      assert_select 'input[name=?]', 'club[name]'

      assert_select 'input[name=?]', 'club[country_code]'

      assert_select 'input[name=?]', 'club[city]'
    end
  end
end
