# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'clubs/show' do
  before do
    @club = assign(:club, Club.create!(
      name: 'Name',
      country_code: 'Country Code',
      city: 'City'
    ))
  end

  it 'renders attributes in <p>' do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/Country Code/)
    expect(rendered).to match(/City/)
  end
end
