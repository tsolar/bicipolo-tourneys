# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'clubs/index' do
  before do
    assign(:clubs, [
             Club.create!(
               name: 'Name',
               country_code: 'Country Code',
               city: 'City'
             ),
             Club.create!(
               name: 'Name',
               country_code: 'Country Code',
               city: 'City'
             )
           ])
  end

  it 'renders a list of clubs' do
    render
    cell_selector = Rails::VERSION::STRING >= '7' ? 'div>p' : 'tr>td'
    assert_select cell_selector, text: Regexp.new('Name'.to_s), count: 2
    assert_select cell_selector, text: Regexp.new('Country Code'.to_s), count: 2
    assert_select cell_selector, text: Regexp.new('City'.to_s), count: 2
  end
end
