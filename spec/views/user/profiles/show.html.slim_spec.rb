# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'user/profiles/show' do
  before do
    assign(:user_profile, create(:profile))
  end

  it 'renders attributes in <p>' do
    pending 'login!'
    render
    expect(rendered).to match(/First Name/)
    expect(rendered).to match(/Middle Name/)
    expect(rendered).to match(/Last Name/)
    expect(rendered).to match(/Second Last Name/)
    expect(rendered).to match(/Nickname/)
    expect(rendered).to match(/Dob/)
    expect(rendered).to match(/Country Code/)
    expect(rendered).to match(/City/)
    expect(rendered).to match(/Club/)
    expect(rendered).to match(/Shirt Size/)
    expect(rendered).to match(/Lefty/)
    expect(rendered).to match(/Gender/)
    expect(rendered).to match(/Other Gender/)
    expect(rendered).to match(/Pronouns/)
    expect(rendered).to match(/Created At/)
    expect(rendered).to match(/Updated At/)
    expect(rendered).to match(/User/)
  end
end
