# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'user/profiles/edit' do
  let(:user_profile) do
    create(:profile)
  end

  before do
    assign(:user_profile, user_profile)
  end

  it 'renders the edit user_profile form' do
    pending 'login!'
    render

    assert_select 'form[action=?][method=?]', user_profile_path, 'post' do
      assert_select 'input[name=?]', 'user_profile[first_name]'

      assert_select 'input[name=?]', 'user_profile[middle_name]'

      assert_select 'input[name=?]', 'user_profile[last_name]'

      assert_select 'input[name=?]', 'user_profile[second_last_name]'

      assert_select 'input[name=?]', 'user_profile[nickname]'

      assert_select 'input[name=?]', 'user_profile[dob]'

      assert_select 'input[name=?]', 'user_profile[country_code]'

      assert_select 'input[name=?]', 'user_profile[city]'

      assert_select 'input[name=?]', 'user_profile[club_id]'

      assert_select 'input[name=?]', 'user_profile[shirt_size]'

      assert_select 'input[name=?]', 'user_profile[lefty]'

      assert_select 'input[name=?]', 'user_profile[gender]'

      assert_select 'input[name=?]', 'user_profile[other_gender]'

      assert_select 'input[name=?]', 'user_profile[pronouns]'

      assert_select 'input[name=?]', 'user_profile[created_at]'

      assert_select 'input[name=?]', 'user_profile[updated_at]'

      assert_select 'input[name=?]', 'user_profile[user_id]'
    end
  end
end
