# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'teams/index' do
  before do
    pending
    assign(:teams, [
             Team.create!(
               name: 'Name',
               tournament: nil
             ),
             Team.create!(
               name: 'Name',
               tournament: nil
             )
           ])
  end

  it 'renders a list of teams' do
    render
    cell_selector = Rails::VERSION::STRING >= '7' ? 'div>p' : 'tr>td'
    assert_select cell_selector, text: Regexp.new('Name'.to_s), count: 2
    assert_select cell_selector, text: Regexp.new(nil.to_s), count: 2
  end
end
