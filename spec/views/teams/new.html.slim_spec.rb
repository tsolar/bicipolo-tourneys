# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'teams/new' do
  before do
    pending
    assign(:team, Team.new(
      name: 'MyString',
      tournament: nil
    ))
  end

  it 'renders new team form' do
    render

    assert_select 'form[action=?][method=?]', teams_path, 'post' do
      assert_select 'input[name=?]', 'team[name]'

      assert_select 'input[name=?]', 'team[tournament_id]'
    end
  end
end
