# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'teams/edit' do
  before do
    pending
    @team = assign(:team, Team.create!(
      name: 'MyString',
      tournament: nil
    ))
  end

  it 'renders the edit team form' do
    render

    assert_select 'form[action=?][method=?]', team_path(@team), 'post' do
      assert_select 'input[name=?]', 'team[name]'

      assert_select 'input[name=?]', 'team[tournament_id]'
    end
  end
end
