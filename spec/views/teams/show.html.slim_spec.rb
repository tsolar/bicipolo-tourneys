# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'teams/show' do
  before do
    pending
    @team = assign(:team, Team.create!(
      name: 'Name',
      tournament: nil
    ))
  end

  it 'renders attributes in <p>' do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(//)
  end
end
