# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'rounds/new' do
  before do
    assign(:round, Round.new(
      name: 'MyString',
      order: 1,
      tournament: nil
    ))
  end

  it 'renders new round form' do
    render

    assert_select 'form[action=?][method=?]', rounds_path, 'post' do
      assert_select 'input[name=?]', 'round[name]'

      assert_select 'input[name=?]', 'round[order]'

      assert_select 'input[name=?]', 'round[tournament_id]'
    end
  end
end
