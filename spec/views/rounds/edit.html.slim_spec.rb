# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'rounds/edit' do
  before do
    pending
    @round = assign(:round, Round.create!(
      name: 'MyString',
      order: 1,
      tournament: nil
    ))
  end

  it 'renders the edit round form' do
    render

    assert_select 'form[action=?][method=?]', round_path(@round), 'post' do
      assert_select 'input[name=?]', 'round[name]'

      assert_select 'input[name=?]', 'round[order]'

      assert_select 'input[name=?]', 'round[tournament_id]'
    end
  end
end
