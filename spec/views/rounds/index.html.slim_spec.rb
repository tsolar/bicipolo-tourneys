# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'rounds/index' do
  before do
    pending
    assign(:rounds, [
             Round.create!(
               name: 'Name',
               order: 2,
               tournament: nil
             ),
             Round.create!(
               name: 'Name',
               order: 2,
               tournament: nil
             )
           ])
  end

  it 'renders a list of rounds' do
    render
    cell_selector = Rails::VERSION::STRING >= '7' ? 'div>p' : 'tr>td'
    assert_select cell_selector, text: Regexp.new('Name'.to_s), count: 2
    assert_select cell_selector, text: Regexp.new(2.to_s), count: 2
    assert_select cell_selector, text: Regexp.new(nil.to_s), count: 2
  end
end
