# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'rounds/show' do
  before do
    pending
    @round = assign(:round, Round.create!(
      name: 'Name',
      order: 2,
      tournament: nil
    ))
  end

  it 'renders attributes in <p>' do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/2/)
    expect(rendered).to match(//)
  end
end
