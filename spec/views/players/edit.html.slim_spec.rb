# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'players/edit' do
  before do
    pending
    @player = assign(:player, Player.create!(
      name: 'MyString',
      nickname: 'MyString',
      team: nil
    ))
  end

  it 'renders the edit player form' do
    render

    assert_select 'form[action=?][method=?]', player_path(@player), 'post' do
      assert_select 'input[name=?]', 'player[name]'

      assert_select 'input[name=?]', 'player[nickname]'

      assert_select 'input[name=?]', 'player[team_id]'
    end
  end
end
