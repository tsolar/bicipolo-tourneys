# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'players/show' do
  before do
    pending
    @player = assign(:player, Player.create!(
      name: 'Name',
      nickname: 'Nickname',
      team: nil
    ))
  end

  it 'renders attributes in <p>' do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/Nickname/)
    expect(rendered).to match(//)
  end
end
