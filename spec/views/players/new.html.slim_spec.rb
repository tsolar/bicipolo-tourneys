# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'players/new' do
  before do
    pending
    assign(:player, Player.new(
      name: 'MyString',
      nickname: 'MyString',
      team: nil
    ))
  end

  it 'renders new player form' do
    render

    assert_select 'form[action=?][method=?]', players_path, 'post' do
      assert_select 'input[name=?]', 'player[name]'

      assert_select 'input[name=?]', 'player[nickname]'

      assert_select 'input[name=?]', 'player[team_id]'
    end
  end
end
