# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'tournaments/edit' do
  before do
    pending
    @tournament = assign(:tournament, create(:tournament))
  end

  it 'renders the edit tournament form' do
    render

    assert_select 'form[action=?][method=?]', tournament_path(@tournament), 'post' do
      assert_select 'input[name=?]', 'tournament[name]'

      assert_select 'textarea[name=?]', 'tournament[description]'

      assert_select 'input[name=?]', 'tournament[address]'

      assert_select 'input[name=?]', 'tournament[city]'

      assert_select 'input[name=?]', 'tournament[country_code]'

      assert_select 'input[name=?]', 'tournament[latitude]'

      assert_select 'input[name=?]', 'tournament[longitude]'
    end
  end
end
