# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'tournaments/show' do
  before do
    pending
    @tournament = assign(:tournament, create(:tournament))
  end

  it 'renders attributes in <p>' do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/Address/)
    expect(rendered).to match(/City/)
    expect(rendered).to match(/Country Code/)
    expect(rendered).to match(/2.5/)
    expect(rendered).to match(/3.5/)
  end
end
