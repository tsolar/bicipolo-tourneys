# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'tournaments/index' do
  before do
    pending
    assign(:tournaments, [
             Tournament.create!(
               name: 'Name',
               description: 'MyText',
               address: 'Address',
               city: 'City',
               country_code: 'Country Code',
               latitude: 2.5,
               longitude: 3.5
             ),
             Tournament.create!(
               name: 'Name',
               description: 'MyText',
               address: 'Address',
               city: 'City',
               country_code: 'Country Code',
               latitude: 2.5,
               longitude: 3.5
             )
           ])
  end

  it 'renders a list of tournaments' do
    render
    cell_selector = Rails::VERSION::STRING >= '7' ? 'div>p' : 'tr>td'
    assert_select cell_selector, text: Regexp.new('Name'.to_s), count: 2
    assert_select cell_selector, text: Regexp.new('MyText'.to_s), count: 2
    assert_select cell_selector, text: Regexp.new('Address'.to_s), count: 2
    assert_select cell_selector, text: Regexp.new('City'.to_s), count: 2
    assert_select cell_selector, text: Regexp.new('Country Code'.to_s), count: 2
    assert_select cell_selector, text: Regexp.new(2.5.to_s), count: 2
    assert_select cell_selector, text: Regexp.new(3.5.to_s), count: 2
  end
end
