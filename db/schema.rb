# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.2].define(version: 2024_09_18_064408) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "pgcrypto"
  enable_extension "plpgsql"

  create_table "clubs", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.string "country_code"
    t.string "city"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "identification_document_types", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.string "internal_name"
    t.string "country_code"
    t.string "validation_regex"
    t.string "format_example"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "identification_documents", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "identification_document_type_id", null: false
    t.string "number"
    t.uuid "profile_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["identification_document_type_id"], name: "index_id_doc_on_id_doc_type_id"
    t.index ["profile_id"], name: "index_identification_documents_on_profile_id"
  end

  create_table "match_fouls", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "match_id", null: false
    t.uuid "tournament_team_player_id", null: false
    t.string "time"
    t.boolean "offensive", default: false, null: false
    t.string "foul_type"
    t.string "foul_order"
    t.string "seconds_out"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["match_id"], name: "index_match_fouls_on_match_id"
    t.index ["tournament_team_player_id"], name: "index_match_fouls_on_tournament_team_player_id"
  end

  create_table "match_goals", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "match_id", null: false
    t.uuid "tournament_team_player_id", null: false
    t.string "time"
    t.boolean "own_goal", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["match_id"], name: "index_match_goals_on_match_id"
    t.index ["tournament_team_player_id"], name: "index_match_goals_on_tournament_team_player_id"
  end

  create_table "matches", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "round_id", null: false
    t.uuid "home_team_id", null: false
    t.uuid "visitor_team_id", null: false
    t.integer "home_score"
    t.integer "visitor_score"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["home_team_id"], name: "index_matches_on_home_team_id"
    t.index ["round_id"], name: "index_matches_on_round_id"
    t.index ["visitor_team_id"], name: "index_matches_on_visitor_team_id"
  end

  create_table "players", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.string "nickname"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.uuid "club_id"
    t.uuid "profile_id"
    t.index ["club_id"], name: "index_players_on_club_id"
    t.index ["profile_id"], name: "index_players_on_profile_id"
  end

  create_table "profiles", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "first_name"
    t.string "middle_name"
    t.string "last_name"
    t.string "second_last_name"
    t.string "nickname"
    t.date "dob"
    t.string "country_code"
    t.string "city"
    t.uuid "club_id"
    t.string "shirt_size"
    t.boolean "lefty", default: false, null: false
    t.string "gender"
    t.string "other_gender"
    t.string "pronouns"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.uuid "user_id"
    t.string "food_preferences"
    t.string "allergies"
    t.string "preferred_locale", default: "en", null: false
    t.index ["club_id"], name: "index_profiles_on_club_id"
    t.index ["user_id"], name: "index_profiles_on_user_id"
  end

  create_table "rounds", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.integer "order"
    t.uuid "tournament_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["tournament_id"], name: "index_rounds_on_tournament_id"
  end

  create_table "teams", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.uuid "tournament_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["tournament_id"], name: "index_teams_on_tournament_id"
  end

  create_table "tournament_team_players", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "tournament_id", null: false
    t.uuid "team_id"
    t.uuid "player_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "captain", default: false, null: false
    t.index ["player_id"], name: "index_tournament_team_players_on_player_id"
    t.index ["team_id"], name: "index_tournament_team_players_on_team_id"
    t.index ["tournament_id", "player_id"], name: "index_tournament_team_players_on_tournament_id_and_player_id", unique: true
    t.index ["tournament_id"], name: "index_tournament_team_players_on_tournament_id"
  end

  create_table "tournaments", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.datetime "starts_at"
    t.datetime "ends_at"
    t.string "address"
    t.string "city"
    t.string "country_code"
    t.decimal "latitude", precision: 10, scale: 6
    t.decimal "longitude", precision: 10, scale: 6
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "team_players_limit"
  end

  create_table "users", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.integer "failed_attempts", default: 0, null: false
    t.string "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["unlock_token"], name: "index_users_on_unlock_token", unique: true
  end

  add_foreign_key "identification_documents", "identification_document_types"
  add_foreign_key "identification_documents", "profiles"
  add_foreign_key "match_fouls", "matches"
  add_foreign_key "match_fouls", "tournament_team_players"
  add_foreign_key "match_goals", "matches"
  add_foreign_key "match_goals", "tournament_team_players"
  add_foreign_key "matches", "rounds"
  add_foreign_key "matches", "teams", column: "home_team_id"
  add_foreign_key "matches", "teams", column: "visitor_team_id"
  add_foreign_key "players", "clubs"
  add_foreign_key "players", "profiles"
  add_foreign_key "profiles", "clubs"
  add_foreign_key "profiles", "users"
  add_foreign_key "rounds", "tournaments"
  add_foreign_key "teams", "tournaments"
  add_foreign_key "tournament_team_players", "players"
  add_foreign_key "tournament_team_players", "teams"
  add_foreign_key "tournament_team_players", "tournaments"
end
