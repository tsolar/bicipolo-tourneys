# frozen_string_literal: true

class CreateMatchFouls < ActiveRecord::Migration[7.0]
  def change
    create_table :match_fouls, id: :uuid do |t|
      t.references :match, null: false, foreign_key: true, type: :uuid
      t.references :tournament_team_player, null: false, foreign_key: true, type: :uuid
      t.string :time
      t.boolean :offensive, default: false, null: false
      t.string :foul_type
      t.string :foul_order
      t.string :seconds_out

      t.timestamps
    end
  end
end
