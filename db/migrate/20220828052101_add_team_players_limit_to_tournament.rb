# frozen_string_literal: true

class AddTeamPlayersLimitToTournament < ActiveRecord::Migration[7.0]
  def change
    add_column :tournaments, :team_players_limit, :integer
  end
end
