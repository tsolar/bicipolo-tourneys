# frozen_string_literal: true

class CreateRounds < ActiveRecord::Migration[7.0]
  def change
    create_table :rounds, id: :uuid do |t|
      t.string :name
      t.integer :order
      t.references :tournament, null: false, foreign_key: true, type: :uuid

      t.timestamps
    end
  end
end
