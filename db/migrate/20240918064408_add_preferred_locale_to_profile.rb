# frozen_string_literal: true

class AddPreferredLocaleToProfile < ActiveRecord::Migration[7.2]
  def change
    add_column :profiles, :preferred_locale, :string, null: false, default: 'en'
  end
end
