# frozen_string_literal: true

class AddCaptainToTournamentTeamPlayers < ActiveRecord::Migration[7.0]
  def change
    add_column :tournament_team_players, :captain, :boolean, default: false, null: false
  end
end
