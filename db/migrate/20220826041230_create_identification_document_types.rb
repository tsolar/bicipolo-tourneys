# frozen_string_literal: true

class CreateIdentificationDocumentTypes < ActiveRecord::Migration[7.0]
  def change
    create_table :identification_document_types, id: :uuid do |t|
      t.string :name
      t.string :internal_name
      t.string :country_code
      t.string :validation_regex
      t.string :format_example

      t.timestamps
    end
  end
end
