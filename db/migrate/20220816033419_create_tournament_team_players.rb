# frozen_string_literal: true

class CreateTournamentTeamPlayers < ActiveRecord::Migration[7.0]
  def change
    create_table :tournament_team_players, id: :uuid do |t|
      t.references :tournament, null: false, foreign_key: true, type: :uuid
      t.references :team, foreign_key: true, type: :uuid
      t.references :player, null: false, foreign_key: true, type: :uuid

      t.timestamps
    end

    add_index :tournament_team_players, %i[tournament_id player_id], unique: true
  end
end
