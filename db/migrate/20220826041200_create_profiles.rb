# frozen_string_literal: true

class CreateProfiles < ActiveRecord::Migration[7.0]
  def change
    create_table :profiles, id: :uuid do |t|
      t.string :first_name
      t.string :middle_name
      t.string :last_name
      t.string :second_last_name
      t.string :nickname
      t.date :dob
      t.string :country_code
      t.string :city
      t.references :club, foreign_key: true, type: :uuid
      t.string :shirt_size
      t.boolean :lefty, default: false, null: false
      t.string :gender
      t.string :other_gender
      t.string :pronouns

      t.timestamps
    end
  end
end
