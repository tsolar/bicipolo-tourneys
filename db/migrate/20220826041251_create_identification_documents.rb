# frozen_string_literal: true

class CreateIdentificationDocuments < ActiveRecord::Migration[7.0]
  def change
    create_table :identification_documents, id: :uuid do |t|
      t.references(
        :identification_document_type,
        null: false,
        foreign_key: true,
        index: { name: 'index_id_doc_on_id_doc_type_id' },
        type: :uuid
      )
      t.string :number
      t.references :profile, null: false, foreign_key: true, type: :uuid

      t.timestamps
    end
  end
end
