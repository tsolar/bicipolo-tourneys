# frozen_string_literal: true

class CreateTeams < ActiveRecord::Migration[7.0]
  def change
    create_table :teams, id: :uuid do |t|
      t.string :name
      t.references :tournament, foreign_key: true, type: :uuid

      t.timestamps
    end
  end
end
