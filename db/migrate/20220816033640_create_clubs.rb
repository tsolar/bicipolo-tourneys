# frozen_string_literal: true

class CreateClubs < ActiveRecord::Migration[7.0]
  def change
    create_table :clubs, id: :uuid do |t|
      t.string :name
      t.string :country_code
      t.string :city

      t.timestamps
    end
  end
end
