# frozen_string_literal: true

class AddFoodPreferencesAndAllergiesToProfile < ActiveRecord::Migration[7.2]
  def change
    change_table :profiles, bulk: true do |t|
      t.string :food_preferences
      t.string :allergies
    end
  end
end
