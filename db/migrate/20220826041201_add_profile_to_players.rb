# frozen_string_literal: true

class AddProfileToPlayers < ActiveRecord::Migration[7.0]
  def change
    add_reference :players, :profile, foreign_key: true, type: :uuid
  end
end
