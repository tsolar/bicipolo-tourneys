# frozen_string_literal: true

class CreateMatchGoals < ActiveRecord::Migration[7.0]
  def change
    create_table :match_goals, id: :uuid do |t|
      t.references :match, null: false, foreign_key: true, type: :uuid
      t.references :tournament_team_player, null: false, foreign_key: true, type: :uuid
      t.string :time
      t.boolean :own_goal, default: false, null: false

      t.timestamps
    end
  end
end
