# frozen_string_literal: true

class AddUserToProfile < ActiveRecord::Migration[7.2]
  def change
    add_reference :profiles, :user, foreign_key: true, type: :uuid
  end
end
