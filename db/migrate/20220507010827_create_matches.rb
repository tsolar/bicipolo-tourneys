# frozen_string_literal: true

class CreateMatches < ActiveRecord::Migration[7.0]
  def change
    create_table :matches, id: :uuid do |t|
      t.references :round, null: false, foreign_key: true, type: :uuid
      t.references :home_team, null: false, foreign_key: { to_table: :teams }, type: :uuid
      t.references :visitor_team, null: false, foreign_key: { to_table: :teams }, type: :uuid
      t.integer :home_score
      t.integer :visitor_score

      t.timestamps
    end
  end
end
